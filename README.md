# Foobar

Foobar is a Python library for dealing with word pluralization.

## Installation LOCAL

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash

pip install local

```

## Usage

```python
import foobar

# returns 'words'
foobar.pluralize('word')

# returns 'geese'
foobar.pluralize('goose')

# returns 'phenomenon'
local.singularize('phenomena')

```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.


## Encore du travail 

ready to serv